const admin = require('firebase-admin');
const serviceAccount = require('./drinkwine-2327f-firebase-adminsdk-fy4tl-707f724a00.json');


if (!admin.apps.length) {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
    });
}
const dbAdmin = admin.firestore();
module.exports = { admin, dbAdmin };

