import DefaultEntityType from "./default";

export default interface User extends DefaultEntityType {
    nome: string,
    cpf: string,
    email: string,
    senha?: string | undefined;    
    grupo: string
    ativo: boolean
    desabilitado?:false
}