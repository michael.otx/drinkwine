import { NextPage } from "next";
import { useState, useEffect } from "react";
import UserDB from "@/public/database/wrappers/user";
import User from "@/public/database/entities/user.entity";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column'; 
import { InputText } from 'primereact/inputtext';   
import { Card } from 'primereact/card'; 
import { Paginator } from 'primereact/paginator';    
import { Button } from 'primereact/button';
import { InputSwitch } from 'primereact/inputswitch';
import { Skeleton } from 'primereact/skeleton';   
import '../app.css';      
import './index.css';
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";

const BuscarUsuario: NextPage = () => {
    const router = useRouter(); 
    const [first, setFirst] = useState(0);
    const [rows, setRows] = useState(10);
    const [searchTerm, setSearchTerm] = useState('');
    const [searchResults, setSearchResults] = useState<User[]>([]);
    const [totalRecords, setTotalRecords] = useState(0);
    const [currentPage, setCurrentPage] = useState(0); 
    const [loading, setLoading] = useState(true);
    const { data: session } = useSession();



    useEffect(() => { 
        if (!session) {
            router.push('/login');
        }
    }, [session, router]);


    useEffect(() => {
        async function fetchUsers() {
            try {
                const users = await new UserDB().getAll();

                const filteredUsers = users.filter(user =>
                    removeAccents(user.nome.toLowerCase()).includes(removeAccents(searchTerm.toLowerCase()))
                );

                setTotalRecords(filteredUsers.length);

                const startIndex = currentPage * rows;
                const endIndex = startIndex + rows;
                const usersForPage = filteredUsers.slice(startIndex, endIndex);

                setSearchResults(usersForPage);

                setLoading(false);
            } catch (error) {
                console.error('Erro ao buscar usuários:', error);
            }
        }

        fetchUsers();
    }, [searchTerm, currentPage, rows]);

    const onPageChange = (event)  => {
        setFirst(event.first);
        setCurrentPage(event.first / rows);
    };

    const removeAccents = (str: string) => {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    };


    const handleEdit = () => {
        router.push('/edit');
    };

    const items = Array.from({ length: 10 }, (v, i) => i);

    const handleDesativar = async (user: User) => {
        const confirmation = window.confirm("Tem certeza de que deseja desativar este usuário?");
        if (confirmation) {
            try {
                await new UserDB().update(user.id, { ...user, ativo: false });
        
                setSearchResults(prevResults =>
                    prevResults.map(prevUser =>
                        prevUser.id === user.id ? { ...prevUser, ativo: false } : prevUser
                    )
                );
            } catch (error) {
                console.error('Erro ao desativar usuário:', error);
            }
        }
    };
    
    const handleAtivar = async (user: User) => {
        const confirmation = window.confirm("Tem certeza de que deseja ativar este usuário?");
        if (confirmation) {
            try {
                await new UserDB().update(user.id, { ...user, ativo: true });
        
                setSearchResults(prevResults =>
                    prevResults.map(prevUser =>
                        prevUser.id === user.id ? { ...prevUser, ativo: true } : prevUser
                    )
                );
            } catch (error) {
                console.error('Erro ao ativar usuário:', error);
            }
        }
    };

    return (
        <div>
            <Card className="card-list-members h-full">
                <h1>Lista de Usuários</h1>

                <div className="mt-5">
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText 
                            placeholder="Digite o nome do usuário" 
                            value={searchTerm} 
                            onChange={(e) => setSearchTerm(e.target.value)}
                        />
                    </span>
                </div>

                <div className="mt-5">
                    {!loading && (
                        <DataTable value={searchResults} showGridlines tableStyle={{ minWidth: '50rem' }} className="table-users mb-2">
                            <Column field="nome" header="Nome"></Column>
                            <Column field="email" header="Email"></Column>
                            <Column field="grupo" header="Grupo"></Column>
                            <Column header="Status" body={(rowData: any) => (
                                <span className={rowData.ativo ? "status-ativo" : "status-inativo"}>
                                    {rowData.ativo ? "Ativo" : "Inativo"}
                                </span>)}>
                            </Column>
                            <Column header="Ação" body={(rowData: any) => (
                                <InputSwitch checked={rowData.ativo} onChange={() => rowData.ativo ? handleDesativar(rowData) : handleAtivar(rowData)} />)}>
                            </Column>
                            <Column header="Alterar" body={(rowData: any) => (
                                <Button icon="pi pi-user-edit" className="p-button-info" onClick={handleEdit} />
                            )} />

                        </DataTable>
                    )}
                    {loading && (
                        <DataTable value={items} className="p-datatable-striped">
                            <Column header="Nome" style={{ width: '250px', height: '70px' }} body={<Skeleton />}></Column>
                            <Column header="Email" style={{  width: '250px', height: '70px' }} body={<Skeleton />}></Column>
                            <Column header="Grupo" style={{  width: '250px', height: '70px' }} body={<Skeleton />}></Column>
                            <Column header="Status" style={{  width: '250px', height: '70px' }} body={<Skeleton />}></Column>
                            <Column header="Ação" style={{  width: '250px', height: '70px' }} body={<Skeleton />}></Column>
                            <Column header="Alterar" style={{  width: '250px', height: '70px' }} body={<Skeleton />}></Column>
                        </DataTable>
                    )}
                </div>

            </Card>
            <div className="card paginator">
                <Paginator first={first} rows={rows} totalRecords={totalRecords} onPageChange={onPageChange} />
            </div>
        </div>
    );
};

export default BuscarUsuario;
