import { NextPage } from "next";
import { useState } from "react";
import { signIn } from 'next-auth/react';
import { useRouter } from "next/router";
import { Card } from 'primereact/card';
import { InputText } from 'primereact/inputtext';
import { Password } from 'primereact/password';
import { Button } from 'primereact/button';
import '../app.css';
import './index.css';
import { useSession } from 'next-auth/react';
import UserDB from "@/public/database/wrappers/user";

const Login: NextPage = () => {
    const { data: session } = useSession();
    let userSession: any = session;
    const router = useRouter();
    const [email, setEmail] = useState<string>('');
    const [senha, setSenha] = useState<string>('');
    const [emailError, setEmailError] = useState<string>('');
    const [senhaError, setSenhaError] = useState<string>('');
    const [loginError, setLoginError] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);

    const handleLogin = async (email: string, senha: string) => {
        try {
            setLoading(true);
            const user = await new UserDB().getByEmail(email);
            const res = await signIn('credentials', {
                email: email,
                password: senha,
                redirect: false,
            });

            if (res?.ok) {
                console.log(userSession,' aWDUIEF')
                if (user && !user.ativo) {
                    setLoginError('Usuário não encontrado ou inativo . Por favor, verifique seu email e senha.');
                    return;
                }
                if (user && user.ativo && user.grupo === 'admin') {
                    router.push('/backoffice');
                } else if (user && user.ativo && user.grupo === 'estoquista') {
                    router.push('/backofficeEstoquista');

                }
            }
        } finally {
            setLoading(false);
        }
    }

    return (
        <div className="cont">
            <Card className="card-login">
                <div className="card-login-header">
                    <h1>Seja bem-vindo</h1>
                    <p>Insira seus dados e acesse a plataforma</p>
                </div>

                <form onSubmit={(e) => { e.preventDefault(); handleLogin(email, senha); }}>
                    <div className="flex gap-4 flex-wrap justify-content-center">
                        <div className="flex flex-column gap-2">
                            <label htmlFor="email">E-mail</label>
                            <InputText value={email} onChange={e => setEmail(e.target.value)} />
                            {setLoginError && <div className="text-danger mb-3">{setLoginError}</div>}
                        </div>

                        <div className="flex flex-column gap-2">
                            <label htmlFor="senha">Senha</label>
                            <Password value={senha} onChange={e => setSenha(e.target.value)} toggleMask feedback={false} />
                            {senhaError && <div className="text-danger mb-3">{senhaError}</div>}
                        </div>
                        {loginError && <div className="text-danger" style={{ width: '300px', textAlign: 'center' }}>{loginError}</div>}
                    </div>
                    <div className="mt-6 flex justify-content-center">
                        <Button
                            label="Entrar"
                            disabled={loading}
                            className="btn-login"
                            loading={loading}
                            iconPos={"right"}
                        />
                    </div>
                </form>
            </Card>
        </div>
    );
};

export default Login;
