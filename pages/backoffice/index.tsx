import { NextPage } from "next";
import { useRouter } from "next/router"; 
import { signOut } from "next-auth/react";

import { useSession } from "next-auth/react";
import { Card } from 'primereact/card';
import { Avatar } from 'primereact/avatar';
import { Ripple } from 'primereact/Ripple';
import { Button } from "primereact/button";
import '../app.css';
import './index.css';
import { auth } from "@/public/database/firebase";
import { useEffect } from "react";

const Backoffice: NextPage = () => {
    const router = useRouter(); 
    
    const { data: session } = useSession(); 

    useEffect(() => { 
        if (!session) {
            router.push('/login');
        }
    }, [session, router]);


    const BuscaUsuario = () => {
        router.push('/buscarUsuario');
    }
    const sair = () => {
        signOut();
        router.push('/login');
    }



    return (
        <div className="cont">
            <div className="sidebar py-3">
                <ul className="list-none p-0 m-0 overflow-hidden">
                    <li onClick={BuscaUsuario}>
                        <a className="p-ripple flex align-items-center cursor-pointer p-3 
                        border-round text-700 hover:surface-100 transition-duration-150 transition-colors w-full">
                            <i className="pi pi-users mr-2" style={{ fontSize: '1.1rem' }}></i>
                            <span className="font-medium">Lista de Úsuarios</span>
                            <Ripple />
                        </a>
                    </li>
                    <li>
                        <a className="p-ripple flex align-items-center cursor-pointer p-3 
                        border-round text-700 hover:surface-100 transition-duration-150 transition-colors w-full">
                            <i className="pi pi-shopping-bag mr-2" style={{ fontSize: '1.1rem' }}></i>
                            <span className="font-medium">Lista de Prdutos</span>
                            <Ripple />
                        </a>
                    </li>
                </ul>
                <div className="mt-auto w-full flex flex-column align-items-center justify-content-center">
                    <hr className="w-full border-top-1 border-none surface-border" />
                    <a v-ripple className="flex align-items-center justify-content-center cursor-pointer p-3 gap-2 border-round text-700 
                    hover:surface-100 transition-duration-150 transition-colors p-ripple">
                        <Avatar label="B" shape="circle" className="avatar-user" />
                        <span className="name-user">Backoffice</span>
                    </a>
                    <Button onClick={sair}  label="Sair" icon="pi pi-sign-out" iconPos="right" className="btn-logout" />
                </div>
            </div>
            <Card className="card-backoffice">
                <h1 className="mt-7">Bem vindo ao Backoffice</h1>
            </Card>
        </div>
    );
};

export default Backoffice;
