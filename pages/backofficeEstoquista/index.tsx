import { signOut } from "next-auth/react";
import { NextPage } from "next";
import router from "next/router";
import { Ripple } from "primereact/Ripple";
import { Avatar } from "primereact/avatar";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import { auth } from "@/public/database/firebase";
import '../app.css';
import './index.css';

const backofficeEstoquista: NextPage = () => {


    const sair = () => {
        signOut();
        router.push('/login');
    }


    return (
        <div className="cont">
            <div className="sidebar py-3">
                <ul className="list-none p-0 m-0 overflow-hidden">

                    <li>
                        <a className="p-ripple flex align-items-center cursor-pointer p-3 
                        border-round text-700 hover:surface-100 transition-duration-150 transition-colors w-full">
                            <i className="pi pi-shopping-bag mr-2" style={{ fontSize: '1.1rem' }}></i>
                            <span className="font-medium">Lista de Produtos</span>
                            <Ripple />
                        </a>
                    </li>
                </ul>
                <div className="mt-auto w-full flex flex-column align-items-center justify-content-center">
                    <hr className="w-full border-top-1 border-none surface-border" />
                    <a v-ripple className="flex align-items-center justify-content-center cursor-pointer p-3 gap-2 border-round text-700 
                    hover:surface-100 transition-duration-150 transition-colors p-ripple">
                        <Avatar label="B" shape="circle" className="avatar-user" />
                        <span className="name-user">Backoffice</span>
                    </a>
                    <Button onClick={sair} label="Sair" icon="pi pi-sign-out" iconPos="right" className="btn-logout" />
                </div>
            </div>
            <Card className="card-backoffice">
                <h1 className="mt-7">Bem vindo ao Backoffice</h1>
            </Card>
        </div>
    );
};


export default backofficeEstoquista;
