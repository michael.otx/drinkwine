import { PrimeReactProvider } from 'primereact/api';
import { SessionProvider } from 'next-auth/react';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import '/node_modules/primeflex/primeflex.css'
import 'primeicons/primeicons.css';
import { AppProps } from 'next/app';

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <SessionProvider>
      <PrimeReactProvider>
        <Component {...pageProps} />
      </PrimeReactProvider>
    </SessionProvider >
  );
}

export default MyApp;
