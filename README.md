# Ecommerce de Vinho em Next.js com Firebase

Este é um projeto de ecommerce de vinho desenvolvido em Next.js, utilizando Firebase para o gerenciamento de dados, PrimeReact para a interface do usuário e NextAuth para a autenticação.

## Sobre o Projeto

Este projeto é uma plataforma de ecommerce desenvolvida em Next.js, uma estrutura de aplicativo da web React de alto desempenho, integrada com Firebase para o armazenamento de dados, PrimeReact para os componentes de interface do usuário e NextAuth para a autenticação de usuários.

## Funcionalidades

- Listagem de produtos de vinho
- Páginas de detalhes do produto
- Carrinho de compras
- Checkout
- Autenticação de usuário com NextAuth
- Gerenciamento de pedidos
- Gerenciamento de estoque para administradores e estoquistas
- Interface de administração para administradores

## Tecnologias Utilizadas

- [Next.js](https://nextjs.org/) - Estrutura de aplicativo da web React
- [Firebase](https://firebase.google.com/) - Plataforma de desenvolvimento de aplicativos da web e móveis
- [PrimeReact](https://www.primefaces.org/primereact/) - Biblioteca de componentes React
- [NextAuth](https://next-auth.js.org/) - Biblioteca de autenticação para Next.js

## Pré-requisitos

- Node.js >= 12.x
- npm ou yarn